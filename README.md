# NdiFrontend

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.20.

# Deploiement 

# Le Site

Le site dispose de 5 fonctionnalités principales :

## Le chatbot

Le chatbot se lance a n'importe quel moment grace au bouton en bas a droite de l'écran. Celui-ci permet de dialoguer avec un robot, afin d'obtenir des informations relatives à nos droit, et comment utiliser le site web.

## Le Guide

Le guide est un ensemble de page qui regroupe toute les informations nécéssaires à un étudiant. Les données sont tiré du site https://campus.univ-grenoble-alpes.fr/.
Nous nous en somme servi pour dresser un catalogue (pas exhaustif mais presque), de tout ce qui concerne les étudiants de l'universite Grenoble-Alpes.
Nous avons regroupé toutes ces informations dans différentes catégorie. Ainsi, si un nouvel étudiant arrive à Grenoble sans rien connaitre des démarches administratives qu'il doit effectuer, ou de ses droits, il n'aura qu'a se réferer aux différentes rubriques le concernant.

## La Carte

L'onglet Carte montre la carte centrée sur Grenoble avec les points d'interets tels que la CAF, l'Hopital ou encore la TAG.

## Le Matchmaking

L'onglet Match permet a un etudiant qui cherche des information de trouver une personne réelle qui pourrait lui transmettre les informations recherché. Si un etudiant recherche des information sur les APL par exemple, il pourra matcher avec quelqu'un qui connait les APL.
Si on "match" avec une personne, on est alors redirigé sur la messagerie, et on peut tchater en privé avec cette personne, afin d'obtenir des informations sur l'objet de notre recherche.

## La Messagerie

Notre site dispose aussi d'un système de messagerie instantannée pour permettre une entraide entre plusieurs personnes.
Il y a des channels publiques, centrée sur une thématique. Plusieurs personnes peuvent rejoindre ces channels. Il y a aussi la possibilité de chatter par message privée, avec une seule personne.
