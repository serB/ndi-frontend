// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  backend: "http://localhost:3000",
  firebase: {
    apiKey: "AIzaSyC0OB3oh8UWavTNgy5WAp1qhE83MuPwTso",
    authDomain: "ndi-2019.firebaseapp.com",
    databaseURL: "https://ndi-2019.firebaseio.com",
    projectId: "ndi-2019",
    storageBucket: "ndi-2019.appspot.com",
    messagingSenderId: "311299246138",
    appId: "1:311299246138:web:0e65af0870ec7fc8a28d03"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
