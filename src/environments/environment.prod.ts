export const environment = {
  production: true,
  backend: "http://localhost:3000",
  firebase: {
    apiKey: "AIzaSyC0OB3oh8UWavTNgy5WAp1qhE83MuPwTso",
    authDomain: "ndi-2019.firebaseapp.com",
    databaseURL: "https://ndi-2019.firebaseio.com",
    projectId: "ndi-2019",
    storageBucket: "ndi-2019.appspot.com",
    messagingSenderId: "311299246138",
    appId: "1:311299246138:web:0e65af0870ec7fc8a28d03"
  }
};
