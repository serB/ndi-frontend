import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RepererComponent } from './reperer.component';

describe('RepererComponent', () => {
  let component: RepererComponent;
  let fixture: ComponentFixture<RepererComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RepererComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RepererComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
