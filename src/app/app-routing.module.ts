import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomePageComponent } from './home-page/home-page.component';
import { LoginComponent } from './login/login.component';
import { MapComponent } from './map/map.component';
import { MatchingComponent } from './matching/matching.component';
import { BudgetComponent } from './budget/budget.component';

import { LogementComponent} from './logement/logement.component';
import { ConseilsComponent} from './logement/conseils/conseils.component';
import { AidesComponent} from './logement/aides/aides.component';
import { DemarchesComponent} from './logement/demarches/demarches.component';
import { DroitsComponent} from './logement/droits/droits.component';
import { LieuComponent} from './logement/lieu/lieu.component';
import { TemporaireComponent} from './logement/temporaire/temporaire.component';

import { SanteBienEtreComponent} from './sante-bien-etre/sante-bien-etre.component';

import { TransportsComponent} from './transports/transports.component';
import { CommunComponent} from './transports/commun/commun.component';
import { InternationalComponent} from './transports/international/international.component';
import { RepererComponent} from './transports/reperer/reperer.component';
import { VeloComponent} from './transports/velo/velo.component';
import { VoitureComponent} from './transports/voiture/voiture.component';

import { CommercesComponent} from './vie-pratique/commerces/commerces.component';
import { EcoResponsableComponent} from './vie-pratique/eco-responsable/eco-responsable.component';
import { MultiservicesComponent} from './vie-pratique/multiservices/multiservices.component';
import { ObjetsTrouvesComponent} from './vie-pratique/objets-trouves/objets-trouves.component';
import { RestaurationComponent} from './vie-pratique/restauration/restauration.component';
import {InternetComponent} from './vie-pratique/internet/internet.component';
import {BienEtreComponent} from './sante-bien-etre/bien-etre/bien-etre.component';
import {EcouteComponent} from './sante-bien-etre/ecoute/ecoute.component';
import {HandicapComponent} from './sante-bien-etre/handicap/handicap.component';
import {SoinsOffreComponent} from './sante-bien-etre/soins-offre/soins-offre.component';
import {SoinsSystemeComponent} from './sante-bien-etre/soins-systeme/soins-systeme.component';
import { ViePratiqueComponent } from './vie-pratique/vie-pratique.component';
import { BudgetEtudiantComponent } from './budget/budget-etudiant/budget-etudiant.component';
import { BoursesEtudesComponent } from './budget/bourses-etudes/bourses-etudes.component';
import { AidesLogementComponent } from './budget/aides-logement/aides-logement.component';
import { JobsEtudiantsComponent } from './budget/jobs-etudiants/jobs-etudiants.component';
import { TaxesImpotsComponent } from './budget/taxes-impots/taxes-impots.component';
import { DifficultesComponent } from './budget/difficultes/difficultes.component';
import { MessagingComponent } from './messaging/messaging.component';


const routes: Routes = [
  {
    path: '',
    component: HomePageComponent
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'map',
    component: MapComponent
  },
  {
    path: 'matching',
    component: MatchingComponent
  },
  {
    path: 'budget',
    component: BudgetComponent
  },
  {
    path: 'budget/budget-etudiant',
    component: BudgetEtudiantComponent
  },
  {
    path: 'budget/bourses-etudes',
    component: BoursesEtudesComponent
  },
  {
    path: 'budget/aides-logement',
    component: AidesLogementComponent
  },
  {
    path: 'budget/jobs-etudiants',
    component: JobsEtudiantsComponent
  },
  {
    path: 'budget/taxes-impots',
    component: TaxesImpotsComponent
  },
  {
    path: 'budget/difficultes',
    component: DifficultesComponent
  },
  {
    path: 'logement',
    component: LogementComponent
  },
  {
    path: 'logement/conseils',
    component: ConseilsComponent
  },
  {
    path: 'logement/aides',
    component: AidesComponent
  },
  {
    path: 'logement/demarches',
    component: DemarchesComponent
  },
  {
    path: 'logement/droits',
    component: DroitsComponent
  },
  {
    path: 'logement/lieu',
    component: LieuComponent
  },
  {
    path: 'logement/temporaire',
    component: TemporaireComponent
  },
  {
    path: 'sante-bien-etre',
    component: SanteBienEtreComponent
  },
  {
    path: 'sante-bien-etre/bien-etre',
    component: BienEtreComponent
  },
  {
    path: 'sante-bien-etre/ecoute',
    component: EcouteComponent
  },
  {
    path: 'sante-bien-etre/handicap',
    component: HandicapComponent
  },
  {
    path: 'sante-bien-etre/soins-offre',
    component: SoinsOffreComponent
  },
  {
    path: 'sante-bien-etre/soins-systeme',
    component: SoinsSystemeComponent
  },
  {
    path: 'transport',
    component: TransportsComponent
  },
  {
    path: 'transport/commun',
    component: CommunComponent
  },
  {
    path: 'transport/international',
    component: InternationalComponent
  },
  {
    path: 'transport/reperer',
    component: RepererComponent
  },
  {
    path: 'transport/velo',
    component: VeloComponent
  },
  {
    path: 'vie-pratique',
    component: ViePratiqueComponent
  },
  {
    path: 'vie-pratique/eco-responsable',
    component: EcoResponsableComponent
  },
  {
    path: 'vie-pratique/multiservices',
    component: MultiservicesComponent
  },
  {
    path: 'vie-pratique/objets-trouves',
    component: ObjetsTrouvesComponent
  },
  {
    path: 'vie-pratique/restauration',
    component: RestaurationComponent
  },
  {
    path: 'vie-pratique/internet',
    component: InternetComponent
  },
  {
    path: 'vie-pratique/commerces',
    component: CommercesComponent
  },
  {
    path: 'messaging',
    component: MessagingComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
