import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';
import * as leafmap from 'leaflet';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnInit {
  db;
  markers = [];

  constructor(db: AngularFirestore) {
    this.db = db;
  }

  ngOnInit() {
    var map = leafmap.map('map', {
      center: [45.1841602, 5.680523],
      zoom: 13
    });

    const tiles = leafmap.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      maxZoom: 19,
      attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
    });

    tiles.addTo(map);
    var marker;
    var c = true;
    this.db.collection('/map')
      .valueChanges()
      .subscribe(res => {
        res.forEach(e => {
          var m = leafmap.marker([e['position'].longitude, e['position'].latitude], {
            icon: leafmap.icon({
              iconSize: [25, 41],
              iconAnchor: [13, 41],
              iconUrl: 'leaflet/marker-icon.png',
              shadowUrl: 'leaflet/marker-shadow.png'
            }),
            title: 'aze'
          }).addTo(map);
          m.bindPopup(e['description']);
          this.markers.push(m);
        });
      });
  }
}
