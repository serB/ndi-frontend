import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SoinsSystemeComponent } from './soins-systeme.component';

describe('SoinsSystemeComponent', () => {
  let component: SoinsSystemeComponent;
  let fixture: ComponentFixture<SoinsSystemeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SoinsSystemeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SoinsSystemeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
