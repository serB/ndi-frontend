import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SanteBienEtreComponent } from './sante-bien-etre.component';

describe('SanteBienEtreComponent', () => {
  let component: SanteBienEtreComponent;
  let fixture: ComponentFixture<SanteBienEtreComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SanteBienEtreComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SanteBienEtreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
