import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SoinsOffreComponent } from './soins-offre.component';

describe('SoinsOffreComponent', () => {
  let component: SoinsOffreComponent;
  let fixture: ComponentFixture<SoinsOffreComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SoinsOffreComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SoinsOffreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
