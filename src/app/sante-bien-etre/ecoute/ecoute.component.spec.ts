import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EcouteComponent } from './ecoute.component';

describe('EcouteComponent', () => {
  let component: EcouteComponent;
  let fixture: ComponentFixture<EcouteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EcouteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EcouteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
