import { Component, OnInit } from '@angular/core';
import { AuthService } from './services/auth.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'ndi-frontend';
  loginModal = false;
  isNavBarCollapsed: boolean;
  user: any;
  fakeUser: any = {
    Name: "Name",
    LastName: "LastName",
    someUserInfo: "Profile Description"
  }
  authentificated: boolean;

  constructor(private auth: AuthService, public router: Router) {
  }

  ngOnInit(): void {
    this.auth.getUserState().subscribe(val => {
      this.authentificated = val != null;
      this.user = val;
      this.isNavBarCollapsed = false;
    });
  }

  getUserMail() {
    return this.user ? this.user.email : null;
  }

  openLogin() {
    this.loginModal = true;
  }

  closeLogin() {
    this.loginModal = false;
  }

  doExpand($event) {
    if ($event.srcElement.className === "nav-trigger-icon") {
      if ($event.srcElement.title === "Expand") {
        this.isNavBarCollapsed = true;
      } else if ($event.srcElement.title === "Collapse") {
        this.isNavBarCollapsed = false;
      }
    }
  }
}
