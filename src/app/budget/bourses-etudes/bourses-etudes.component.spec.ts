import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BoursesEtudesComponent } from './bourses-etudes.component';

describe('BoursesEtudesComponent', () => {
  let component: BoursesEtudesComponent;
  let fixture: ComponentFixture<BoursesEtudesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BoursesEtudesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BoursesEtudesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
