import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaxesImpotsComponent } from './taxes-impots.component';

describe('TaxesImpotsComponent', () => {
  let component: TaxesImpotsComponent;
  let fixture: ComponentFixture<TaxesImpotsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaxesImpotsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaxesImpotsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
