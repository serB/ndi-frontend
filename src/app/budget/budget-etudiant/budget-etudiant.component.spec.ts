import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BudgetEtudiantComponent } from './budget-etudiant.component';

describe('BudgetEtudiantComponent', () => {
  let component: BudgetEtudiantComponent;
  let fixture: ComponentFixture<BudgetEtudiantComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BudgetEtudiantComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BudgetEtudiantComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
