import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JobsEtudiantsComponent } from './jobs-etudiants.component';

describe('JobsEtudiantsComponent', () => {
  let component: JobsEtudiantsComponent;
  let fixture: ComponentFixture<JobsEtudiantsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JobsEtudiantsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JobsEtudiantsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
