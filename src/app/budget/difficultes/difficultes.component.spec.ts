import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DifficultesComponent } from './difficultes.component';

describe('DifficultesComponent', () => {
  let component: DifficultesComponent;
  let fixture: ComponentFixture<DifficultesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DifficultesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DifficultesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
