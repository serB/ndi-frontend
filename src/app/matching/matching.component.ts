import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-matching',
  templateUrl: './matching.component.html',
  styleUrls: ['./matching.component.scss']
})
export class MatchingComponent implements OnInit {
  parentSubject: Subject<string> = new Subject();
  isAvailable = [true];
  constructor() { }

  ngOnInit() {
  }
  cardAnimation(value) {
    this.parentSubject.next(value);
  }

}
