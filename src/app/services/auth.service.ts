import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';

import { map } from 'rxjs/operators';
import { HttpClient } from "@angular/common/http";
import { environment } from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class AuthService {


  BACKEND = environment.backend;

  constructor(private auth: AngularFireAuth, private http: HttpClient) { 
  }

  async login(email: string, password: string) {
    try {
      await this.auth.auth.signInWithEmailAndPassword(email, password);
      return true;
    } catch (error) {
      return error;
    }
  }


  async register(props){
    try {
      await this.http.post(this.BACKEND+"/user", props).toPromise();
      return true
    } catch (error) {
      return error['error'];
    } 
  }

  getUserState(){
    return this.auth.authState;
  }

  getUser(){
    return this.auth.auth.currentUser;
  }

  isConnected(): boolean{
    return this.auth.auth.currentUser!=null;
  }

  logout(){
    this.auth.auth.signOut();
  }



}
