import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';

import { AuthService } from "../services/auth.service";


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})

export class LoginComponent implements OnInit {


  email = "";
  password = "";
  prenom = "";
  is_helping = false;
  expertise = {};
  ville = "";
  age = 0;
  picture = "";

  successRegister = false;

  error;
  @Input() mode = "login";

  form = new FormGroup({
    email: new FormControl('', Validators.required),
    password: new FormControl('', Validators.required),
  });

  @Output() public onComplete: EventEmitter<any> = new EventEmitter();

  constructor(private auth: AuthService) { }

  ngOnInit() {
    
  }

  async submitLogin() {
    let res = await this.auth.login(this.email, this.password)
    if (this.auth.isConnected()) {
      this.onComplete.emit();
    }
    else
      this.error = res["message"];
  }
  async submitRegister() {
    let expertise_array = Object.values(this.expertise);
    let props = {
      email: this.email,
      password: this.password,
      prenom: this.prenom,
      ville: this.ville,
      is_helping: this.is_helping,
      expertise: expertise_array,
      picture: this.picture,
      age: 0
    };
    let res = await this.auth.register(props);
    if (res == true) {
      this.changeMode();
      this.successRegister = true;
    }
    else
      this.error = res["message"];
  }

  changeMode(){
    this.mode = this.mode==="login" ? "register":"login";
  }

}
