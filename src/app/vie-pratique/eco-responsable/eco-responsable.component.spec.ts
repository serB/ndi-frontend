import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EcoResponsableComponent } from './eco-responsable.component';

describe('EcoResponsableComponent', () => {
  let component: EcoResponsableComponent;
  let fixture: ComponentFixture<EcoResponsableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EcoResponsableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EcoResponsableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
