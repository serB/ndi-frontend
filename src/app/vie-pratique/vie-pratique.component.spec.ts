import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViePratiqueComponent } from './vie-pratique.component';

describe('ViePratiqueComponent', () => {
  let component: ViePratiqueComponent;
  let fixture: ComponentFixture<ViePratiqueComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViePratiqueComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViePratiqueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
