import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ObjetsTrouvesComponent } from './objets-trouves.component';

describe('ObjetsTrouvesComponent', () => {
  let component: ObjetsTrouvesComponent;
  let fixture: ComponentFixture<ObjetsTrouvesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ObjetsTrouvesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ObjetsTrouvesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
