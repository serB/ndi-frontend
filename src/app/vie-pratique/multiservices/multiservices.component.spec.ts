import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MultiservicesComponent } from './multiservices.component';

describe('MultiservicesComponent', () => {
  let component: MultiservicesComponent;
  let fixture: ComponentFixture<MultiservicesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MultiservicesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MultiservicesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
