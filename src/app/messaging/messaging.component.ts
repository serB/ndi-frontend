import { Component, OnInit, ViewChild, ElementRef, Input } from '@angular/core';
import Chatkit from '@pusher/chatkit-client';
import axios from 'axios';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-messaging',
  templateUrl: './messaging.component.html',
  styleUrls: ['./messaging.component.scss']
})

export class MessagingComponent implements OnInit {
  userId = 'LeLeo';
  userName = 'LeoJan';
  currentUser = <any>{};
  messages = [];
  currentRoom = <any>{};
  roomUsers = [];
  userRooms = [];
  newMessage = '';
  newRoom = {
    name: '',
    isPrivate: false
  };
  joinableRooms = [];
  newUser = '';
  isDM = false;
  // idRoom = 'v1:us1:1ae194ba-6a9c-43c2-81d4-3cb17baa30b8';
  @ViewChild('chatDiv', { static: false }) chatDiv: ElementRef;
  @Input()
  userToConnectTo: any = {};
  isFromMatch = false;
  idRoom = 'aecf6e6e-7e41-4ca8-8d38-95da9954a737'; // Room "Général"

  constructor(private route: ActivatedRoute) {
    this.route.queryParams.subscribe(params => {
      if (params.id !== '' && params.name !== '' && params.id != null && params.name != null) {
        this.userToConnectTo = params;
        this.isFromMatch = true;
      }
      // this.createAndConnectUser();
    });

  }

  ngOnInit(): void {
    this.createAndConnectUser();
  }

  addUserToRoom() {
    const { newUser, currentUser, currentRoom } = this;

    currentUser.addUserToRoom({
      userId: newUser,
      roomId: currentRoom.id
    })
      .then((currentRoom) => {
        this.roomUsers = currentRoom.users;
      })
      .catch(err => {
        console.log(`Error adding user: ${err}`);
      });

    this.newUser = '';
  }

  createRoom() {
    const { newRoom: { name, isPrivate }, currentUser } = this;

    if (name.trim() === '') { return; }

    currentUser.createRoom({
      name,
      private: isPrivate,
    }).then(room => {
      this.connectToRoom(room.id);
      this.newRoom = {
        name: '',
        isPrivate: false,
      };
      this.idRoom = room.id;
    })
      .catch(err => {
        console.log(`Error creating room ${err}`);
      });
  }

  getJoinableRooms() {
    const { currentUser } = this;
    currentUser.getJoinableRooms()
      .then(rooms => {
        this.joinableRooms = rooms;
      })
      .catch(err => {
        console.log(`Error getting joinable rooms: ${err}`);
      });
  }

  joinRoom(id) {
    const { currentUser } = this;
    currentUser.joinRoom({ roomId: id })
      .catch(err => {
        console.log(`Error joining room ${id}: ${err}`);
      });
  }

  connectToRoom(id) {
    this.messages = [];
    const { currentUser } = this;

    currentUser.subscribeToRoom({
      roomId: `${id}`,
      messageLimit: 100,
      hooks: {
        onMessage: message => {
          this.messages.push(message);
        },
        onPresenceChanged: () => {
          this.roomUsers = this.currentRoom.users.sort((a) => {
            if (a.presence.state === 'online') { return -1; }

            return 1;
          });
        },
      },
    })
      .then(currentRoom => {
        this.currentRoom = currentRoom;
        this.roomUsers = currentRoom.users;
        this.userRooms = currentUser.rooms;
        if (this.isDM) {
          this.addUserToRoom();
          // console.log('fdfdfdf');
          this.isDM = false;
        }
      });
  }

  sendMessage() {
    const { newMessage, currentUser, currentRoom } = this;

    if (newMessage.trim() === '') { return; }

    currentUser.sendMessage({
      text: newMessage,
      roomId: `${currentRoom.id}`,
    });

    this.newMessage = '';
    this.chatDiv.nativeElement.scrollTop = this.chatDiv.nativeElement.scrollHeight;
  }

  createAndConnectUser() {
    const { userId, userName } = this;
    axios.post('http://localhost:5200/users', { userId, userName })
      .then(() => {
        const tokenProvider = new Chatkit.TokenProvider({
          url: 'http://localhost:5200/authenticate'
        });

        const chatManager = new Chatkit.ChatManager({
          instanceLocator: 'v1:us1:1ae194ba-6a9c-43c2-81d4-3cb17baa30b8',
          userId,
          tokenProvider
        });

        return chatManager
          .connect({
            onAddedToRoom: room => {
              console.log(`Added to room ${room.name}`);
              this.userRooms.push(room);
              this.getJoinableRooms();
            },
          })
          .then(currentUser => {
            this.currentUser = currentUser;
            this.getJoinableRooms();
            if (this.isFromMatch) {
              this.userRooms = currentUser.rooms;
              this.createPrivateChatRoom(this.userToConnectTo);
              console.log(this.userToConnectTo);
              this.isFromMatch = false;
            } else {
              this.connectToRoom(this.idRoom);
            }
          });
      })
      .catch(error => console.error(error));
  }

  createUser(userId, userName) {
    axios.post('http://localhost:5200/users', { userId, userName })
      .then(() => {
        const tokenProvider = new Chatkit.TokenProvider({
          url: 'http://localhost:5200/authenticate'
        });

        const chatManager = new Chatkit.ChatManager({
          instanceLocator: 'v1:us1:1ae194ba-6a9c-43c2-81d4-3cb17baa30b8',
          userId,
          tokenProvider
        });
      }
      );
  }

  createPrivateChatRoom(userToConnectWith) {
    let found = false;
    if (userToConnectWith.id !== this.userId) {
      for (let i = 0; i < this.userRooms.length; i++) {
        if (this.userRooms[i].name === ('[CHAT] ' + userToConnectWith.name) && !found) {
          found = true;
          this.connectToRoom(this.userRooms[i].id);
        }
      }

      if (!found) {
        this.isDM = true;
        this.newRoom = { name: '[CHAT] ' + userToConnectWith.name, isPrivate: true };
        this.newUser = userToConnectWith.id;
        this.createRoom();
      }
    }
  }
}

