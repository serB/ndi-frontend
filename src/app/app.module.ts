import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ClarityModule } from '@clr/angular';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HomePageComponent } from './home-page/home-page.component';
import { MatchingComponent } from './matching/matching.component';
import { CardComponent } from './card/card.component';

import { environment } from '../environments/environment';

import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { MapComponent } from './map/map.component';
import { BudgetComponent } from './budget/budget.component';
import { LogementComponent } from './logement/logement.component';
import { TransportsComponent } from './transports/transports.component';
import { ViePratiqueComponent } from './vie-pratique/vie-pratique.component';
import { SanteBienEtreComponent } from './sante-bien-etre/sante-bien-etre.component';
import { ConseilsComponent } from './logement/conseils/conseils.component';
import { LieuComponent } from './logement/lieu/lieu.component';
import { TemporaireComponent } from './logement/temporaire/temporaire.component';
import { DemarchesComponent } from './logement/demarches/demarches.component';
import { AidesComponent } from './logement/aides/aides.component';
import { DroitsComponent } from './logement/droits/droits.component';
import { RepererComponent } from './transports/reperer/reperer.component';
import { CommunComponent } from './transports/commun/commun.component';
import { VeloComponent } from './transports/velo/velo.component';
import { VoitureComponent } from './transports/voiture/voiture.component';
import { InternationalComponent } from './transports/international/international.component';
import { RestaurationComponent } from './vie-pratique/restauration/restauration.component';
import { CommercesComponent } from './vie-pratique/commerces/commerces.component';
import { MultiservicesComponent } from './vie-pratique/multiservices/multiservices.component';
import { ObjetsTrouvesComponent } from './vie-pratique/objets-trouves/objets-trouves.component';
import { EcoResponsableComponent } from './vie-pratique/eco-responsable/eco-responsable.component';
import { InternetComponent } from './vie-pratique/internet/internet.component';
import { BienEtreComponent } from './sante-bien-etre/bien-etre/bien-etre.component';
import { SoinsOffreComponent } from './sante-bien-etre/soins-offre/soins-offre.component';
import { SoinsSystemeComponent } from './sante-bien-etre/soins-systeme/soins-systeme.component';
import { HandicapComponent } from './sante-bien-etre/handicap/handicap.component';
import { EcouteComponent } from './sante-bien-etre/ecoute/ecoute.component';
import { BudgetEtudiantComponent } from './budget/budget-etudiant/budget-etudiant.component';
import { BoursesEtudesComponent } from './budget/bourses-etudes/bourses-etudes.component';
import { AidesLogementComponent } from './budget/aides-logement/aides-logement.component';
import { JobsEtudiantsComponent } from './budget/jobs-etudiants/jobs-etudiants.component';
import { TaxesImpotsComponent } from './budget/taxes-impots/taxes-impots.component';
import { DifficultesComponent } from './budget/difficultes/difficultes.component';
import { MessagingComponent } from './messaging/messaging.component';
import { AuthService } from './services/auth.service';
import { LoginComponent } from './login/login.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';


@NgModule({
  declarations: [
    AppComponent,
    HomePageComponent,
    LoginComponent,
    MapComponent,
    MatchingComponent,
    CardComponent,
    LogementComponent,
    TransportsComponent,
    ViePratiqueComponent,
    SanteBienEtreComponent,
    ConseilsComponent,
    LieuComponent,
    TemporaireComponent,
    DemarchesComponent,
    AidesComponent,
    DroitsComponent,
    RepererComponent,
    CommunComponent,
    VeloComponent,
    VoitureComponent,
    InternationalComponent,
    RestaurationComponent,
    CommercesComponent,
    MultiservicesComponent,
    ObjetsTrouvesComponent,
    EcoResponsableComponent,
    InternetComponent,
    BienEtreComponent,
    SoinsOffreComponent,
    SoinsSystemeComponent,
    HandicapComponent,
    EcouteComponent,
    BudgetEtudiantComponent,
    BoursesEtudesComponent,
    AidesLogementComponent,
    JobsEtudiantsComponent,
    TaxesImpotsComponent,
    DifficultesComponent,
    MessagingComponent,
    BudgetComponent,
  ],
  imports: [
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    AngularFireAuthModule,
    BrowserModule,
    AppRoutingModule,
    ClarityModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [AuthService],
  bootstrap: [AppComponent]
})
export class AppModule { }
