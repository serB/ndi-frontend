import { animate, keyframes, transition, trigger } from '@angular/animations';
import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { Subject } from 'rxjs';
import * as kf from './keyframes';


@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css'],
  animations: [
    trigger('cardAnimator', [
      transition('* => swiperight', animate(750, keyframes(kf.swiperight))),
      transition('* => swipeleft', animate(750, keyframes(kf.swipeleft)))
    ])
  ]

})
export class CardComponent implements OnInit, OnDestroy {
  db: AngularFirestore;
  public users = [];
  public index = 0;
  userNull = {
    picture: 'assets/pp/null.jpg'
  };
  @Input()
  parentSubject: Subject<any>;
  @Input()
  isAvailable: boolean[];
  categorie = '';
  animationState: string;
  constructor(db: AngularFirestore, public route: ActivatedRoute, public router: Router) { this.db = db; }

  ngOnInit() {
    this.parentSubject.subscribe(event => {
      this.startAnimation(event);
    });

    this.route.queryParams.subscribe(params => {
      // this.categorie = params.cat;
      if (params.cat != null && params.cat !== '') {
        this.db.collection('/user', ref => ref.where('is_helping', '==', true).where('expertise', 'array-contains', params.cat))
          .valueChanges()
          .subscribe(res => {
            res.forEach(e => {
              this.users.push(e);
            });
          });
      } else {
        this.db.collection('/user', ref => ref.where('is_helping', '==', true))
          .valueChanges()
          .subscribe(res => {
            res.forEach(e => {
              this.users.push(e);
            });
          });
      }
    });

  }

  startAnimation(state) {
    if (!this.animationState) {
      this.animationState = state;
    }
  }

  resetAnimationState(state) {
    if (this.animationState) {
      if (this.index >= this.users.length - 1) {
        this.users.push(this.userNull);
        this.isAvailable[0] = false;
      }
      if (state.toState === 'swiperight') {
        const navigationExtras: NavigationExtras = {
          queryParams: {
            id: 'AdminNDI',
            name: 'Admin'
          }
        };
        console.log(navigationExtras);
        console.log(this.users[this.index]);
        this.router.navigate(['messaging'], navigationExtras);
      }
      this.index++;
      this.animationState = '';
    }
  }

  ngOnDestroy() {
    this.parentSubject.unsubscribe();
  }

}
