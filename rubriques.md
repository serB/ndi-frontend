# Rubriques
Liens : 
* https://campus.univ-grenoble-alpes.fr/
* http://ici-grenoble.org/infospratiques/par-themes.php

## Budget

### Budget étudiant
#### Frais d'inscription
* Frais de scolarité
* CVEC

#### Budget d'installation
* Logement
* Transport

#### Budget du quotidien
* Alimentation
* Tel/internet
* Loisirs

#### Ouvrir un compte en en banque
* Création
* Transfer d'argent international

### Bourses d'études

#### Bourses du CROUS
* Bourse du crous

#### Les bourses pour les étudiants internationaux
* Différentes bourses d'études

#### Les bourses de la région Auvergne-Rhône-Alpes
* Aide a la mobilité internationale
* Aide pour les filières sanitaires et sociale

### Aide au logement

#### CAF
* Caf
* Points d'accueil/ borne interactives de la caf

#### Garanties et cautions locatives
* Visale
* Loca-pass
* cautions locatives bancaires

#### Aides au frais de logement
* tarifs solidaires de l'energie
* fonds de solidarité pour le logement

### Jobs étudiant

#### Préparer sa candidature
* CV 
* Lettre de motiv
* Entretiens

#### Trouver un job
* comment trouver un job
* reseau
* candidature spontanée
* offres

#### Statuts particuliers
* emploi étudiant
* statut étudiant engagé
* service civique

#### Vos droits
* droit du travail
* job et impots
* prime d'activité
* job étudiant/cotisation retraite
* etudiant internationaux/jobs

#### les secteurs qui recrutent

##### milieu universitaires
* crous
* université
* service accueil handicap

##### les autres secteurs
* fonction publique
* animation / périscolaire
* service a la personne
* restauration/hotellerie
* telemarketing
* evenementiel

### Taxes et impots

#### CVEC
* contribution vie etudiante et de campus
* qui est concerné
* remboursement

#### declaration de revenu
* se declarer seul ?
* quoi declarer

#### taxe d'habitation
* taxe d'habitation
* contribution a l'audiovisuel public

### En cas de difficultés

#### difficulté passagères

##### aide sociale du crous
* aide ponctuelle du crous
* aide annuelle du crous

##### aide specifique des etablissement
* dispositif d'aide aux étudiants

#### Exonération des frais d'inscription
* financement des droits d'inscription

#### transports a tarif réduit
* tram/bus
* vélo

#### s'alimenter a moindre frais
* crous grenoble alpes
* l'agoraé
* distribution, colis alimentaires
* lieu de restauration

#### se meubler/vetir/s'equiper
* association étudiantes
* emmaus
* ulisse grenoble solidarité
* la remise
* la croix rouge

## Logement

### conseils pour ma recherche
* conseil avant de s'installer
* trouver un logement décent

### ou trouver un logement

#### logement etudiant crous
* crous gre-alpes

#### autres logement étudiant
* résidence privée et associative
* koloc'a

#### logements partagés
* lokaviz
* parc privé
* colocation intergenerationnelle

#### parc privé
* agences immobilieres
* de particulier a particulier

### les logements temporaires

#### pour quelques nuits
* residence hoteliere ...
* auberge de jeunesse

#### logement temporaires du crous
* crous logement temporaire

#### alternants et stagiaires
* form'toit
* pole habitat insertion jeunes

### les demarches pour emmenager/ déménager

### aides au logement

### vos droits

## Transports

## Vie pratique

## Santé bien-etre

## Culture

## Sports

## Vie associative
